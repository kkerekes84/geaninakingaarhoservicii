FROM python:3.7-slim

COPY pipe.py requirements.txt /
RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "pipe.py"]